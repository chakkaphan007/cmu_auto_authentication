@echo off
setlocal enabledelayedexpansion

:: Proceed with the login process using AutoIt script
start "LoginAutomation" "LoginAutomation.exe"

:: Check if the process is running
tasklist /fi "imagename eq !processName!" | find /i "!processName!" >nul

if %errorlevel% equ 0 (
    echo LoginCMUClient is not running.
) else (
    echo LoginCMUClient is running. Closing it...

    :: Terminate the LoginCMUClient process
    taskkill /im "login-cmu-client.exe" /f
    
    echo LoginCMUClient closed successfully.
)

:: Set the path to the Windows startup folder
set "startupFolder=%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup"

:: Set the name for the shortcut
set "shortcutName=RunLogin"

:: Set the path to the script
set "scriptPath=%~dp0RunLogin.bat"

:: Combine the startup folder path and shortcut name to get the full shortcut path
set "shortcutPath=%startupFolder%\%shortcutName%.lnk"

:: Get the path to the folder containing this script
set "startinPath=%~dp0"

:: Check if the shortcut already exists
if not exist "!shortcutPath!" (
    echo Creating shortcut for RunLogin.bat...

    :: Use PowerShell to create the shortcut
    powershell -Command "$s=(New-Object -COM WScript.Shell).CreateShortcut('!shortcutPath!'); $s.TargetPath='!scriptPath!'; $s.WorkingDirectory='!startinPath!'; $s.Save() "

    echo Shortcut created successfully.
)

:: Proceed with the login process
start "LoginCMUClient" "login-cmu-client.exe"

:: Check the exit code of the last command
if %errorlevel% neq 0 (
    msg * "Error: Incorrect username or password. Please check your credentials."
    taskkill /f /im "login-cmu-client.exe"
    ::goto :eof
) else (
    msg * "Success: Program executed successfully."
)

:end
endlocal
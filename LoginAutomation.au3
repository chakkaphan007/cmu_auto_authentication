; Read credentials from login_credentials.txt
Global $credentialsFile = "login_credentials.txt"
Global $username, $password

; Check if the credentials file exists
If FileExists($credentialsFile) Then
    ; Read the credentials from the file
    $file = FileOpen($credentialsFile)
    $username = FileReadLine($file)
    $password = FileReadLine($file)
    FileClose($file)
Else
    MsgBox(16, "Error", "Credentials file not found.")
    Exit 1 ; Exit with an error code
EndIf

; Check if RBTray is already running
If Not ProcessExists("RBTray.exe") Then
    ; Start RBTray
    Run("RBTray.exe")

    ; Wait for RBTray to initialize (you may need to adjust the sleep duration)
    Sleep(1000)
EndIf
If Not ProcessExists("LoginCMUClient.exe") Then

; Wait for the "LoginCMUClient" window to become active with a timeout of 10 seconds
If WinWaitActive("LoginCMUClient", "", 10) Then
; Send the username and password
Send($username)
Send("{ENTER}")
Sleep(200) ; Adjust as needed
Send($password)
Send("{ENTER}")

; Simulate Control-Alt-Down to minimize LoginCMUClient.exe
Send("^!{DOWN}")
Else
        MsgBox(48, "Error", "Timeout waiting for the LoginCMUClient window to become active.")
EndIf

Else
    MsgBox(64, "Info", "LoginCMUClient.exe is already running.")
EndIf

Exit